# from zalo.sdk.oa import ZaloOaInfo, ZaloOaClient

from zalo.sdk.oa import ZaloOaInfo
from zalo.sdk.oa import ZaloOaClient


zalo_info = ZaloOaInfo(oa_id="4569868130780188512", secret_key="dAeMQAkfvXOl01auOaqv")
zalo_oa_client = ZaloOaClient(zalo_info)
user_id = "7233765607260137444"

def send_message():
    data = {
        'uid': user_id,
        'message': 'hello there'
    }
    params = {'data': data}
    send_text_message = zalo_oa_client.post('/sendmessage/text', params)
    print(send_text_message)

def picture():
    upload_photo_from_path = zalo_oa_client.post('/upload/image', {'file': "img.jpg"})
    print(upload_photo_from_path)
    print(upload_photo_from_path['data']['imageId'])

def send_picture_message():
    # 768a50f0f28e1bd0429f
    data = {
        'uid': user_id,
        'imageid': "768a50f0f28e1bd0429f",
        'message': 'team meow'
    }
    params = {
        'data': data
    }
    send_image_message = zalo_oa_client.post('/sendmessage/image', params)
    print(send_image_message)

picture()