import shutil
import requests
import os
import uuid
# def link_builder(api_key, category):
#     link = "http://thecatapi.com/api/images/get?type=jpg&size=full&api_key=%s&category=%s" % (api_key, category)
#     return link


class TheCatApiClient:

    def __init__(self):
        self.api_key = "MjYyNTQz"
        self.image_dir = "tmp"

    def link_builder(self, category=None):
        if category == None:
            link = "http://thecatapi.com/api/images/get?type=jpg&size=full&api_key=%s" % (
            self.api_key)
        else:
            link = "http://thecatapi.com/api/images/get?type=jpg&size=med&api_key=%s&category=%s" % (self.api_key, category)
        return link

    def link_builder_random(self):
        return "https://api.thecatapi.com/v1/images/search?"

    def request_image(self, category=None):
        url = self.link_builder(category)
        response = requests.get(url, stream=True)
        if not os.path.exists(self.image_dir):
            os.makedirs(self.image_dir)
        file_name = os.path.join(self.image_dir, str(uuid.uuid4())+'.jpg')
        with open(file_name, 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        del response
        return file_name

    def get_redirected_link(self, category=None):
        """
        get img url
        :param category:
        :return:
        """
        url = self.link_builder_random()
        headers = {"Content-Type":"application/json",
                   "x-api-key":"MjYyNTQz"}
        response = requests.get(url, headers=headers)
        url = response.json()[0]["url"]
        return url

client = TheCatApiClient()
x = client.link_builder_random()
r = client.get_redirected_link()
print(x)