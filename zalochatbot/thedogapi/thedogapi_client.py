import shutil

import requests
import os
import uuid
import json

class TheDogApiClient:

    def __init__(self):
        self.api_key = "eb85777f-fa5c-43fc-8e13-2008422757ef"
        self.image_dir = "tmp"
        self.header = {"Content-Type":"application/json", "x-api-key":self.api_key}

    def link_builder(self, category=None):
        # baselink = "https://api.thedogapi.com/v1/images/search?mime_types=image/jpeg";
        baselink = "https://api.thedogapi.com/v1/images/search?";
        link=baselink
        return link

    def parse_link(self, url):
        """
        parse for info from link
        :param url:
        :return: dict of the json
        """
        response = requests.get(url, headers=self.header)
        # print(response.json()[0])
        return response.json()[0]

    def get_redirected_link(self, category=None):
        """
        get img url and dog info
        :param category:
        :return:
        """
        url = self.link_builder(category)
        data = self.parse_link(url)
        mimetype = data["url"].split(".")[-1]
        # while mimetype!= "jpeg" and mimetype!= "jpg":
        #     # retry when not image
        #     data = self.parse_link(url)
        #     mimetype = data["url"].split(".")[-1]
        try:
            doginfo = json.dumps(data["breeds"][0], indent=4, separators=(',', ': '))
        except:
            print("dog info error")
            doginfo = None

        url = data["url"]

        return url, doginfo

    def request_image(self, category=None):
        url = self.link_builder(category)
        data = self.parse_link(url)
        mimetype = data["url"].split(".")[-1]
        while mimetype!= "jpeg" and mimetype!= "jpg":
            # retry when not image
            data = self.parse_link(url)
            mimetype = data["url"].split(".")[-1]
        try:
            doginfo = json.dumps(data["breeds"][0], indent=4, separators=(',', ': '))
        except:
            print("dog info error")
            doginfo = None

        response = requests.get(data["url"], stream=True, headers=self.header)


        if not os.path.exists(self.image_dir):
            os.makedirs(self.image_dir)
        file_name = os.path.join(self.image_dir, str(uuid.uuid4()) + '.jpg')
        with open(file_name, 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        del response
        return file_name, doginfo


if __name__ == "__main__":
    thedogapi = TheDogApiClient()
    link = thedogapi.parse_link(thedogapi.link_builder())
    print(link)
    print(thedogapi.request_image())