import requests
import os
import uuid
import shutil
import random

class UnsplashClient:
    def __init__(self, accessToken=None):

        self.keystore = [
            "8eeefe7682cb65d80ddee7ffe039c77abbe628d4a404e58953c248759df13998",
            "ae65f6e1c35acbeabcd630ce8a8d026759b46227e0bd0b4df31859dd7800dc93",
            "dcc06928b731841b2458a10c51d50938735f5d5897b9616e292536bba9447c5c",
            "f912bc0701a45fe29a1de79bc9e513fbcdcbb8a85d9dd8c5aabb5230a6f134df",
            "48d9b98acf297118e3841b3ef506755ac953de57ef92ae6811b6a5c4607e22bb"
        ]

        if (accessToken==None):
            accessToken = random.choice(self.keystore)
            print("use access token " + accessToken)
        self.accessToken = "Client-ID "+ accessToken
        self.baselink = "https://api.unsplash.com/"
        self.image_dir = "tmp"
        self.header = {"Authorization":self.accessToken}

    def request_random(self,query="aquarium,fish"):
        """
        get random image on unsplash
        :param query:
        :return:
        """
        link = self.baselink+ "photos/random?query=" + "aquarium"
        response = requests.get(link, headers=self.header)
        data = response.json()
        url = data["urls"]["small"]
        html = data["links"]["html"]
        author = data["user"]["name"]
        description = data["description"]
        attribution = "Photo by "+author+" on Unsplash"
        print(data)
        return url, attribution, description, html

    def download_image(self, url):
        """
        download file and save to tmp
        :param url:
        :return:
        """
        response = requests.get(url, stream=True, headers=self.header)

        if not os.path.exists(self.image_dir):
            os.makedirs(self.image_dir)
        file_name = os.path.join(self.image_dir, str(uuid.uuid4()) + '.jpeg')
        with open(file_name, 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        del response
        return file_name

if __name__ == "__main__":
    api =  UnsplashClient()
    url, attr = api.request_random()
    fname = api.download_image(url)
    print(attr)