from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from .zalo.sdk.oa import ZaloOaInfo
from .zalo.sdk.oa import ZaloOaClient
from .thecatapi import TheCatApiClient
import os
from tasks import _task_send_cat, _task_send_dog, _task_send_unsplash


# Create your views here.

@csrf_exempt
def echo(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        return JsonResponse(data)
    elif request.method == "GET":
        data = {"meow": "this is get request"}
        return JsonResponse(data)


@api_view(['GET'])
def read_param(request):
    if request.method == "GET":
        return JsonResponse(request.query_params)

@api_view(['GET'])
def bot_reply_echo(request):
    """
    bot start here
    :param request:
    :return:
    """
    if request.method == "GET":
        param = request.query_params
        fromuid = param['fromuid']
        message = param['message']
        if "cat" in message.lower() or "meow" in message.lower() or "mèo" in message.lower() or "meo" in message.lower():
            try:
                # cat_client = TheCatApiClient()
                # cat_uri = cat_client.request_image()
                # __send_cat(fromuid, cat_uri)
                _task_send_cat.delay(fromuid)
            except Exception as e:
                __send_text(fromuid, e)
        elif "dog" in message.lower() or "woof" in message.lower() or "cho" in message.lower() or "chó" in message.lower():
            try:
                #dog_client = TheDogApiClient()
                #dog_uri, doginfo = dog_client.request_image()
                #__send_dog(fromuid, dog_uri, doginfo)
                _task_send_dog.delay(fromuid)
            except Exception as e:
                __send_text(fromuid, e)
        elif "ca" in message.lower() or "cá" in message.lower() or "fish" in message.lower():
            try:
                # unsplash_client = UnsplashClient()
                # img_url, attribution = unsplash_client.request_random(query="aquarium,fish")
                # img_uri = unsplash_client.download_image(img_url)
                # __send_unsplash(fromuid, img_uri, attribution)
                _task_send_unsplash.delay(fromuid)
            except Exception as e:
                __send_text(fromuid, e)
        elif "help" in message.lower():
            __send_text(fromuid, "gõ câu nói có từ cat, dog, fish để nhận 1 tấm hình chó, mèo, cá bất kỳ (mèo được ưu tiên nhất).  :3 ")
        else:
            __send_text(fromuid, "bạn vừa nói "+ message +  "!!! Hãy gõ help để xem hướng dẫn")
        return JsonResponse(request.query_params)

@api_view(['GET'])
def bot_cat(request):
    if request.method == "GET":
        param = request.query_params
        fromuid = param['fromuid']
        message = param['message']
        try:
            cat_client = TheCatApiClient()
            cat_uri = cat_client.request_image("caturday")
            __send_cat(fromuid, cat_uri)
        except Exception as e:
            __send_text(fromuid, e)
        return JsonResponse(request.query_params)


def __send_text(uid, content):
    # from zalo.sdk.oa import ZaloOaInfo, ZaloOaClient

    zalo_info = ZaloOaInfo(oa_id="2042686217016670007", secret_key="KxQguiABXgoYh3Gy86LQ")
    zalo_oa_client = ZaloOaClient(zalo_info)


    data = {
        'uid': uid,
        'message': content
    }
    params = {'data': data}
    send_text_message = zalo_oa_client.post('/sendmessage/text', params)


def __send_cat(uid, file_name):
    zalo_info = ZaloOaInfo(oa_id="2042686217016670007", secret_key="KxQguiABXgoYh3Gy86LQ")
    zalo_oa_client = ZaloOaClient(zalo_info)
    upload_photo_from_path = zalo_oa_client.post('/upload/image', {'file': file_name})
    print(upload_photo_from_path)
    os.remove(file_name)  # delete tmp file
    data = {
        'uid': uid,
        'imageid': upload_photo_from_path['data']['imageId'],
        'message': "Meowingful day, isn't it ?"
    }
    params = {
        'data': data
    }
    send_image_message = zalo_oa_client.post('/sendmessage/image', params)
    print(send_image_message)


def __send_unsplash(uid, file_name, attribution):
    zalo_info = ZaloOaInfo(oa_id="2042686217016670007", secret_key="KxQguiABXgoYh3Gy86LQ")
    zalo_oa_client = ZaloOaClient(zalo_info)
    upload_photo_from_path = zalo_oa_client.post('/upload/image', {'file': file_name})
    print(upload_photo_from_path)
    os.remove(file_name)  # delete tmp file
    if attribution is not None:
        msg = attribution
    else:
        msg = ""
    data = {
        'uid': uid,
        'imageid': upload_photo_from_path['data']['imageId'],
        'message': msg
    }
    params = {
        'data': data
    }
    send_image_message = zalo_oa_client.post('/sendmessage/image', params)
    print(send_image_message)

def __send_dog(uid, file_name, doginfo):
    zalo_info = ZaloOaInfo(oa_id="2042686217016670007", secret_key="KxQguiABXgoYh3Gy86LQ")
    zalo_oa_client = ZaloOaClient(zalo_info)
    upload_photo_from_path = zalo_oa_client.post('/upload/image', {'file': file_name})
    print(upload_photo_from_path)
    os.remove(file_name)  # delete tmp file
    if doginfo is not None:
        msg = doginfo
    else:
        msg = "woof"
    data = {
        'uid': uid,
        'imageid': upload_photo_from_path['data']['imageId'],
        'message': msg
    }
    params = {
        'data': data
    }
    send_image_message = zalo_oa_client.post('/sendmessage/image', params)
    print(send_image_message)
