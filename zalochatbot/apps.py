from django.apps import AppConfig


class ZalochatbotConfig(AppConfig):
    name = 'zalochatbot'
