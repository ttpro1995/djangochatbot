from django.urls import path
from . import views

urlpatterns = [
    path('echo/', views.echo, name='echo'),
    path('cat/', views.bot_cat, name='cat'),
    path('readparam/', views.read_param, name='param'),
    path('bot/', views.bot_reply_echo, name='bot')
]