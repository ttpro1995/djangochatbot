import celery
import os

import json
from zalochatbot.zalo.sdk.oa import ZaloOaInfo
from zalochatbot.zalo.sdk.oa import ZaloOaClient
from zalochatbot.thecatapi import TheCatApiClient
from zalochatbot.thedogapi import TheDogApiClient
from zalochatbot.theunsplashapi import UnsplashClient
import scout_apm.celery
from scout_apm.api import Config


app = celery.Celery("meowchatbot_worker")
# app.conf.update(BROKER_URL=os.environ['REDIS_URL'],
#                 CELERY_RESULT_BACKEND=os.environ['REDIS_URL'])

app.conf.update(BROKER_URL=os.environ['REDIS_URL'])

Config.set(
        key = 'KoR38xG8ep3uDSv4Q0f6',
        name = 'MeowChatBot',
        monitor = True
        )

scout_apm.celery.install()


#  redis-18604.c60.us-west-1-2.ec2.cloud.redislabs.com:18604
# redis://:DS6Td4zOvfekgVGNPMcXcyZCJ2a5xfRg@redis-18604.c60.us-west-1-2.ec2.cloud.redislabs.com:18604/0
# redis://:DS6Td4zOvfekgVGNPMcXcyZCJ2a5xfRg@redis-18604.c60.us-west-1-2.ec2.cloud.redislabs.com:18604/0
#  export REDIS_URL=redis://:DS6Td4zOvfekgVGNPMcXcyZCJ2a5xfRg@redis-18604.c60.us-west-1-2.ec2.cloud.redislabs.com:18604/0

@app.task
def add(x, y):
    return x + y


def _task_send_cat_old(uid):
    """
    don't use this
    :param uid:
    :return:
    """
    try:
        cat_client = TheCatApiClient()
        cat_uri = cat_client.request_image()
        file_name = cat_uri
        zalo_info = ZaloOaInfo(oa_id="2042686217016670007", secret_key="KxQguiABXgoYh3Gy86LQ")
        zalo_oa_client = ZaloOaClient(zalo_info)
        upload_photo_from_path = zalo_oa_client.post('/upload/image', {'file': file_name})
        ## todo here: handle error
        err_code = int(upload_photo_from_path['errorCode'])
        if err_code!=1:
            print("upload zalo failed")
            # cloud_result = cloudinary.uploader.upload(file_name)
            # image_url = cloud_result["secure_url"]
            image_url = cat_client.get_redirected_link()
            # print(cloud_result)
            links = [{
                'link': image_url,
                'linktitle': "Meowingful day, isn't it ? ",
                'linkdes': '><',
                'linkthumb': image_url
            }]
            data = {
                'uid': uid,
                'links': links
            }
            params = {
                'data': data
            }
            send_link_message = zalo_oa_client.post('/sendmessage/links', params)
            print(send_link_message)
            return send_link_message
        else:
            # send using zalo upload api
            print(upload_photo_from_path)
            os.remove(file_name)  # delete tmp file
            data = {
                'uid': uid,
                'imageid': upload_photo_from_path['data']['imageId'],
                'message': "Meowingful day, isn't it ?"
            }
            params = {
                'data': data
            }
            send_image_message = zalo_oa_client.post('/sendmessage/image', params)
            print(send_image_message)
            return send_image_message
    except Exception as e:
        print(e)
        return "error _task_send_cat"

@app.task
def _task_send_cat(uid):
    try:
        cat_client = TheCatApiClient()
        zalo_info = ZaloOaInfo(oa_id="2042686217016670007", secret_key="KxQguiABXgoYh3Gy86LQ")
        zalo_oa_client = ZaloOaClient(zalo_info)
        image_url = cat_client.get_redirected_link()

        links = [{
            'link': image_url,
            'linktitle': "Meowingful day, isn't it ? ",
            'linkdes': '><',
            'linkthumb': image_url
        }]
        data = {
            'uid': uid,
            'links': links
        }
        params = {
            'data': data
        }
        send_link_message = zalo_oa_client.post('/sendmessage/links', params)
        print(send_link_message)
        return send_link_message

    except Exception as e:
        print(e)
        return "error _task_send_cat"

@app.task
def _task_send_unsplash(uid):
    """
    get photo from unsplash
    :param uid:
    :return:
    """
    try:
        unsplash_client = UnsplashClient()
        img_url, attribution, description, html = unsplash_client.request_random(query="aquarium,fish")


        zalo_info = ZaloOaInfo(oa_id="2042686217016670007", secret_key="KxQguiABXgoYh3Gy86LQ")
        zalo_oa_client = ZaloOaClient(zalo_info)

        if attribution is not None:
            msg = attribution
            title = description
        else:
            title = "><"
            msg = "><"

        links = [{
            'link': html,
            'linktitle': title,
            'linkdes': msg,
            'linkthumb': img_url
        }]
        data = {
            'uid': uid,
            'links': links
        }
        params = {
            'data': data
        }
        send_link_message = zalo_oa_client.post('/sendmessage/links', params)

        print(send_link_message)
        return send_link_message
    except Exception as e:
        print(e)
        return "error _task_send_unsplash"

@app.task
def _task_send_dog(uid):
    try:
        dog_client = TheDogApiClient()
        dog_uri, doginfo = dog_client.get_redirected_link()

        zalo_info = ZaloOaInfo(oa_id="2042686217016670007", secret_key="KxQguiABXgoYh3Gy86LQ")
        zalo_oa_client = ZaloOaClient(zalo_info)

        if doginfo is not None:
            title = json.loads(doginfo)["name"]
            msg = doginfo
        else:
            title = "Woof Woof"
            msg = "woof"

        links = [{
            'link': dog_uri,
            'linktitle': title,
            'linkdes': msg,
            'linkthumb': dog_uri
        }]
        data = {
            'uid': uid,
            'links': links
        }
        params = {
            'data': data
        }
        send_link_message = zalo_oa_client.post('/sendmessage/links', params)
        print(links)
        print(send_link_message)
        return send_link_message

    except Exception as e:
        print(e)
        return "error _task_send_dog"
