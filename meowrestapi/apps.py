from django.apps import AppConfig


class MeowrestapiConfig(AppConfig):
    name = 'meowrestapi'
