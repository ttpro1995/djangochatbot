from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
import json
# Create your views here.

@csrf_exempt
def echo(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        return JsonResponse(data)
    elif request.method == "GET":
        data = {"meow":"this is get request"}
        return JsonResponse(data)

@api_view(['GET'])
def read_param(request):
    if request.method == "GET":
        return JsonResponse(request.query_params)