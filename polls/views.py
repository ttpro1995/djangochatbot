from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def index(request):
    return HttpResponse("Meow,  I am Pusheen the cat")

def detail(request, question_id):
    return HttpResponse("Question %s" % question_id)

def results(request, question_id):
    return HttpResponse("Result of question %s" %question_id)

def vote(request, question_id):
    return HttpResponse("Vote on question %s " %question_id)
